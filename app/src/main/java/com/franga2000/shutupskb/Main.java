package com.franga2000.shutupskb;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;


public class Main implements IXposedHookLoadPackage {
    private static final String HOOK_PACKAGE = "com.halcom.mobile.hybrid.skbasi2xxxx";
    private static String HOOK_CLASS;

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) {

        if (lpparam.packageName.equals(HOOK_PACKAGE)) {

            XposedBridge.log("[SKB] Hooking package " + lpparam.packageName);

            // Hook the method that makes the root detection mandatory
            HOOK_CLASS = "com.halcom.mobile.hybrid.otp.GemaltoApplicationProperties";
            try {
                XposedHelpers.findAndHookMethod(HOOK_CLASS, lpparam.classLoader, "getRootPolicy", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(final MethodHookParam param) {
                        XposedBridge.log("[SKB] Setting root policy to 2");
                        param.setResult(2); // 0 = EXIT, 1 = WARN, 2 = NONE
                    }
                });
                XposedBridge.log("[SKB] Hooked root policy");
            } catch (Throwable e) {
                XposedBridge.log("[SKB] Error hooking: " + HOOK_CLASS + " getRootPolicy");
                e.printStackTrace();
            }

            // Hook the method that performs the root check
            HOOK_CLASS = "com.halcom.mobile.security.otp.GemaltoPinVerifier";
            try {
                XposedHelpers.findAndHookMethod(HOOK_CLASS, lpparam.classLoader, "isDeviceRooted", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(final MethodHookParam param) {
                        XposedBridge.log("[SKB] Root check returned " + param.getResult() + ", forcing to true");
                        param.setResult(false);
                    }
                });
                XposedBridge.log("[SKB] Hooked isDeviceRooted");
            } catch (Throwable e) {
                XposedBridge.log("[SKB] Error hooking: " + HOOK_CLASS + " isDeviceRooted");
                e.printStackTrace();
            }


            // Hook the method that calls the root check
            HOOK_CLASS = "com.halcom.mobile.hybrid.activity.MainActivity";
            try {
                XposedHelpers.findAndHookMethod(HOOK_CLASS, lpparam.classLoader, "rootDeviceCheck", new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(final MethodHookParam param) {
                        param.setResult(null);
                    }
                });
                XposedBridge.log("[SKB] Hooked new root checker");
            } catch (Throwable e) {
                XposedBridge.log("[SKB] Error hooking: " + HOOK_CLASS + " rootDeviceCheck");
                e.printStackTrace();
            }


        }
    }
}
